#!/bin/bash
​: '
This script downloads the public certificate for a website and places it in
a Java keystore.  The URL is the only required argument.
'
url=$1
certname=$2
alias=$3
keystore=$4
password=$5
: ${certname:=tmp.crt}
: ${alias:=website}
: ${password:=changeit}
: ${keystore:=identity.jks}
​
echo -n | openssl s_client -connect $url:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > $certname
​
keytool -importcert -alias $alias -file $certname -keystore $keystore -storepass $password
​
if [ $certname == "tmp.crt" ]
  then
    rm $certname
  fi
