#!/bin/bash
: '
This will generate a private key with password then remove the password.
It will also genrate a CSR (certificate signing request) that you send when
you are purchasing an SSL certificate from someone like GoDaddy.  We are
just creating a selfsigned certificate so the next step just signs it with
our private key and we are done.  Both the server.key and server.crt are used
on the webserver.  The CSR is not used after the certificate is created.
'
# generate key with password
openssl genrsa -des3 -passout pass:x -out server.pass.key 2048

# remove password
openssl rsa -passin pass:x -in server.pass.key -out server.key

# remove key with password
rm server.pass.key

# generate certificate signing request
openssl req -new -key server.key -out server.csr

# create certificate and sign our own key
openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt
