# README #

Josh said this needed a README so I added one.

### What is this repository for? ###

Just a repository to hold random but perhaps useful scripts.  If you have something you've created and think could be useful to someone (including yourself) in the future, then put it here.  I would suggest we avoid one liners in which the appropriate stackoverflow article is only a google away.  That being said, there are some one liners that are the exception and would be appropriate so I leave that to your discretion.

### Organization ###
I've organized the scripts in directories by type.  If there is another way, I'm all ears.

### Warning ###
This is a public repository.  No passwords or other sensitive information.  If in doubt, leave it out.

### Conventions ###
I'll leave this to be organic.  However, I do suggest commenting your scripts more heavily than you may normally do.

### Who do I talk to? ###

* If you are editing/improving someone else's script, talk to them and do a pull request.
* Everyone.  Talk and ask questions.  There are many ways to solve a problem and the right solution often depends on your perspective.
